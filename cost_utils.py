import sys
from sys import exit
from utils import *

import numpy
import matplotlib.pyplot as plt

def load_cost(logfile):
    fin = open(logfile, "r")
    costs = fin.readlines() 
    fin.close()

    costs = map(lambda s: map(lambda ss: eval(ss), s.split(' ')), costs)
    training_cost = map(lambda p: p[0], costs)
    valid_cost = map(lambda p: p[1], costs)

    return training_cost, valid_cost

def find_first_minimal(lst):
    i = 0
    N = len(lst)
    while i + 1 < N:
        if lst[i] < lst[i + 1]:
            break
        i += 1
    return i

