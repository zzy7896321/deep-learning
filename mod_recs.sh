#!/bin/bash

if [[ $# < 6 ]]; then
	echo "usage: $0 <input> <pixels> <prob.> <N> <train_percent> <valid_percent> [keep_tmp=n]"
	exit 1
fi

INPUT=$1
PIXELS=$2
PROB=$3
N=$4
TRAIN_PERCENT=$5
VALID_PERCENT=$6


if [[ $# -ge 6 ]]; then
	KEEP_TMP=$7
else
	KEEP_TMP="n"
fi

INPUT_BASE="$(echo "$INPUT" | sed 's/[.][^.]*$//')"
SUFFIX="$(echo "$INPUT" | sed -n 's/.*[.]\([^.]*\)/\1/p')"

./mod_recs.py $PIXELS 0 "$INPUT" "${INPUT_BASE}_0.${SUFFIX}" "${INPUT_BASE}_0_label.${SUFFIX}"

INPUT_FILES="${INPUT_BASE}_0.${SUFFIX}"
LABEL_FILES="${INPUT_BASE}_0_label.${SUFFIX}"

for (( i = 1; i <= $N; ++i )); do
	./mod_recs.py $PIXELS $PROB "$INPUT" "${INPUT_BASE}_${i}.${SUFFIX}" "${INPUT_BASE}_${i}_label.${SUFFIX}"
	INPUT_FILES="${INPUT_FILES} ${INPUT_BASE}_${i}.${SUFFIX}"
	LABEL_FILES="${LABEL_FILES} ${INPUT_BASE}_${i}_label.${SUFFIX}"
done

INPUT2="${INPUT_BASE}_all.${SUFFIX}"
LABEL2="${INPUT_BASE}_all_label.${SUFFIX}"

cat ${INPUT_FILES} > ${INPUT2}
cat ${LABEL_FILES} > ${LABEL2}

if [[ $KEEP_TMP != "y" ]]; then
	rm $INPUT_FILES
	rm $LABEL_FILES
fi

./split_data.py ${INPUT2} $TRAIN_PERCENT $VALID_PERCENT $PIXELS "true" $LABEL2 $PIXELS


