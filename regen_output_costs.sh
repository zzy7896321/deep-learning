#!/bin/bash

if [[ ! -d output ]]; then
	exit 1
fi

LOGS="$(find -name 'LOGS')"

for logfile in $LOGS; do
	logdir="$(dirname "$logfile")"
	./grep_costs.sh $logfile > "${logdir}/costs.txt"
done
