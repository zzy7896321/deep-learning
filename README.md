Deep Learning Project at Cornell
===================================

1. input_10_1000: 1000 random rectangles in 10x10 grid
2. input_10_11000: corrupt the pixels in the rectangles with prob. 0.1
3. input_10_11000_small: corrupt the pixels in the rectangles with prob. 0.01
4. input_10_11000_large: corrupt the pixels in the rectangles with prob. 0.2


