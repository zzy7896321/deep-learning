#!/usr/bin/env python

import sys
from sys import exit
from utils import *
import random

import os.path

def parse_args(argc, argv):
    if argc < 5 or argc == 6:
        print "usage:", argv[0], "<input> <train_percent> <valid_percent> <lines> <shuffle> [<labelfile> <labelfile_lines>]"
        exit(1)
    
    input_file = argv[1]
    train_percent = parse_float_arg("training percentage", argv[2], lambda x: x >= 0)
    valid_percent = parse_float_arg("validation percentage", argv[3], lambda x: x >= 0)

    if argc >= 4:
        lines = parse_positive_int_arg("lines per data", argv[4])
    else:
        lines = 1

    if argc >= 5:
        shuffle = parse_bool_arg("shuffle", argv[5])
    else:
        shuffle = False
    
    if argc >= 7:
        label_file = argv[6]
        label_lines = parse_positive_int_arg("lines per label", argv[7])
    else:
        label_file = None
        label_lines = None


    test_percent = 1. - train_percent - valid_percent

    if abs(test_percent) < 1e-6:
        test_percent = 0

    if test_percent < 0 or test_percent > 1:
        print "[error] 1 - training percentage - validation percentation is out of range"
        exit(1)

    return input_file, train_percent, valid_percent, lines, shuffle, label_file, label_lines

def count_lines(input_file):
    fin = open(input_file, "r")
    cnt = len(fin.readlines())
    fin.close()

    return cnt

def get_output_names(input_file):
    common = strip_suffix(input_file)

    train = common + "_train.txt"
    valid = common + "_valid.txt"
    test = common + "_test.txt"

    return train, valid, test
   
def split_data_no_shuffle(input_file, train_percent, valid_percent, lines):
    train, valid, test = get_output_names(input_file) 

    count = count_lines(input_file)
    if count % lines != 0:
        print "[WARNING] number of lines % lines per data != 0"

    count /= lines

    train_end = int(count * train_percent) * lines
    valid_end = train_end + int(count * valid_percent) * lines
    
    fin = open(input_file, "r")
    fout = open(train, "w")
    i = 0
    for l in fin:
        if i == train_end:
            fout.close()
            fout = open(valid, "w")
        elif i == valid_end:
            fout.close()
            fout = open(test, "w")

        i += 1
        fout.write(l)
    fout.close()

def split_data_shuffle(input_file, train_percent, valid_percent, lines, label_file, label_lines):
    
    train, valid, test = get_output_names(input_file)
    train = [train]
    valid = [valid]
    test = [test]
    if label_file:
        label_train, label_valid, label_test = get_output_names(label_file)
        train.append(label_train)
        valid.append(label_valid)
        test.append(label_test)
    
    data_set = []
    cur_data = [0] * lines
    now = 0

    fin = open(input_file, "r")
    for line in fin:
        cur_data[now] = line
        now += 1
        if now == lines:
            data_set.append(["".join(cur_data)])
            now = 0
    fin.close()
    if now != 0:
        print "[WARNING] number of lines % lines per data != 0"

    count = len(data_set)

    if label_file:
        cur_data = [0] * label_lines
        now = 0

        fin = open(label_file, "r")
        i = 0
        for line in fin:
            cur_data[now] = line
            now += 1
            if now == label_lines:
                if i >= count:
                    print "[error] the number of data and labels mismatch"
                    exit(1)
                data_set[i].append("".join(cur_data))
                now = 0
                i += 1
        if now != 0 :
            print "[WARNING] number of label lines % labels per line != 0"

    random.shuffle(data_set)

    train_end = int(count * train_percent)
    valid_end = train_end + int(count * valid_percent)
    
    i = 0
    fout = map(lambda fname: open(fname, "w"), train)
    for d in data_set:
        if i == train_end:
            for f in fout:
                f.close()
            fout = map(lambda fname: open(fname, "w"), valid)
        elif i == valid_end:
            for f in fout:
                f.close()
            fout = map(lambda fname: open(fname, "w"), test)

        i += 1
        for (di, fouti) in zip(d, fout):
            fouti.write(di)
    for f in fout:
        f.close()

def main(argc, argv):

    (input_file, 
    train_percent, 
    valid_percent, 
    lines, 
    shuffle,
    label_file,
    label_lines) = parse_args(argc, argv)

    if shuffle:
        split_data_shuffle(input_file, train_percent, valid_percent, lines, label_file, label_lines)
    else:
        split_data_no_shuffle(input_file, train_percent, valid_percent, lines)
        split_data_no_shuffle(label_file, train_percent, valid_percent, label_lines)

if __name__ == "__main__":
    main(len(sys.argv) - 1, sys.argv)
