from sys import exit 
import math
import os.path
import os

def parse_int_arg(name, s, qual = None):
    try:
        value = eval(s)
        if type(value) != int and type(value) != long:
            raise BaseException
        if qual != None and not qual(value):
            raise BaseException
    except:
        print "[error] invalid value of %s: %s" % (name, s)
        exit(1)

    return value

def parse_positive_int_arg(name, s):
    return parse_int_arg(name, s, lambda x: x > 0)

def parse_options_rawstring(name, s, options):
	if s in options:
		return s

	print "[error] invalid value of %s: %s" % (name, s)
	exit(1)

def parse_options(name, s, options):
    try:
        if type(options) == list:
            pos = options.index(s)
        elif type(options) == dict:
            pos = options[s]
        else:
            print "[fatal] unsupported option type"
            exit(1)
    except:
        print "[error] invalid value of %s: %s" % (name, s)
        exit(1)

    return pos

def parse_float_arg(name, s, qual = None):
    try:
        value = eval(s)
        if type(value) == int or type(value) == long:
            value = float(value)

        if type(value) != float:
            raise BaseException
        if qual != None and not qual(value):
            raise BaseException
    except:
        print "[error] invalid value of %s: %s" % (name, s)
        exit(1)

    return value

def format_time_str(t):
    m = int(math.floor(t / 60.))
    s = t - m * 60
    return "%dm %.3fs" % (m, s)

def strip_suffix(path):
    dirname = os.path.dirname(path)
    basename = os.path.basename(path)

    p = basename.rfind('.')
    if p != -1:
            basename = basename[:p]

    path = os.path.join(dirname, basename)
    return path

def parse_bool_arg(name, s):
	s = s.lower()
	if s == "true":
		return True
	elif s == "false":
		return False
	else:
		print "[error] invalid value of %s: %s" % (name, s)

