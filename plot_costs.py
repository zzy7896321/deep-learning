#!/usr/bin/env python

from cost_utils import *

def main(argc, argv):
    if argc < 4:
        print "usage:", argv[0], "<title> <costfile> <outformat> <outfile> [maxrange=20]"
        exit(1)
    
    title = argv[1]
    costfile = argv[2]
    outformat = argv[3]
    outfile = argv[4]

    if argc >= 5:
        maxrange = parse_positive_int_arg("maxrange", argv[5])
    else:
        maxrange = 20

    training_cost, valid_cost = load_cost(costfile)
    N = len(training_cost)
    
    minimal_x = find_first_minimal(valid_cost)
    x_range = min(N - 1, maxrange)
    X = range(x_range + 1) 

    training_line, valid_line = plt.plot(X, training_cost[:x_range+1], "k--*", 
            X, valid_cost[:x_range+1], "b-x")
    plt.title(title, fontsize=24)

    plt.xlabel("Iterations", fontsize = 20)
    plt.ylabel("MSE", fontsize = 20)

    plt.legend([training_line, valid_line] , 
                ["training", "validation"])

    plt.savefig(outfile, format=outformat, dpi=480)

    
    print "%s, %d, %f, %f, %f, %f, %f, %f" % (
            title, minimal_x, 
            training_cost[0], valid_cost[0],
            training_cost[minimal_x], valid_cost[minimal_x], 
            training_cost[-1], valid_cost[-1])
    
if __name__ == "__main__":
    main(len(sys.argv) - 1, sys.argv)
