#!/bin/bash

if [[ $# < 1 ]]; then
	echo "usage: $0 <logfile>"
	exit 1
fi

LOGFILE="$1"

sed -n -e 's/init training cost = \(.*\), valid cost \(.*\)/\1 \2/p' "$LOGFILE"
sed -n -e 's/Training epoch .*, raw_cost \(.*\), training cost .*, valid cost \(.*\)/\1 \2/p' "$LOGFILE"

