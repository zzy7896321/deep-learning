#!/usr/bin/env python

import sys
import random
from utils import *

def parse_args(argv):
    pixels = parse_positive_int_arg("pixels", argv[1])
    N = parse_positive_int_arg("N", argv[2])

    return pixels, N

def get_min_max(x, y):
    if x < y:
        return x, y
    else:
        return y, x
       
def random_gen_recs(pixels, N):
    while N > 0:
        x1, x2 = get_min_max(random.randint(0, pixels - 1), random.randint(0, pixels - 1))
        y1, y2 = get_min_max(random.randint(0, pixels - 1), random.randint(0, pixels - 1))
        print x1, x2, y1, y2
        N -= 1

def gen_all_recs_and_shuffle(pixels, N):
    total = (pixels * (pixels + 1) / 2) ** 2
    lst = [0] * total

    i = 0
    for x1 in xrange(pixels):
        for x2 in xrange(x1, pixels):
            for y1 in xrange(pixels):
                for y2 in xrange(y1, pixels):
                    lst[i] = (x1, x2, y1, y2)
                    i += 1

    random.shuffle(lst)
    
    for i in xrange(N):
        print lst[i][0], lst[i][1], lst[i][2], lst[i][3]

def gen_data(argc, argv):
    if argc < 2 :
        print "Generate rectangles in format (x1, x2, y1, y2)"
        print "usage:", argv[0], "<pixels> <N>"
        sys.exit(1)
    
    pixels, N = parse_args(argv)
    
    total_number_of_recs = (pixels * (pixels + 1) / 2) ** 2
    if N > total_number_of_recs:
        print "[ERROR] N = %d > total_number_of_recs = %d" % (N, total_number_of_recs)
        sys.exit(1)
    
    random.seed()

    if total_number_of_recs > 100000:
        random_gen_recs(pixels, N)
    else:
        gen_all_recs_and_shuffle(pixels, N)

if __name__== "__main__":
    gen_data(len(sys.argv) - 1, sys.argv)

