#!/usr/bin/env python

import sys
from sys import exit

def main(argc, argv):
    if argc < 1:
        print "usage:", argv[0], "<input>"
        exit(1)

    inputfile = argv[1]

    fin = open(inputfile, "r")
    lines = fin.readlines()
    fin.close()
    
    xs = map(lambda x : eval(x), lines)
    xs.sort()
    
    for i in xs:
        print i

if __name__ == "__main__":
    main(len(sys.argv) - 1, sys.argv)
