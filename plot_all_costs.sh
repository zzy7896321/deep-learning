#!/bin/bash

if [[ ! -d output ]]; then
	exit 1
fi

exp_names=(
auto_encoder1
auto_encoder2
auto_encoder_init1
auto_encoder_init2
auto_encoder_init_large1
auto_encoder_init_large2
auto_encoder_init_small1
auto_encoder_init_small2
rec_recover1
rec_recover2
rec_recover_init1
rec_recover_init2
rec_recover_init_large1
rec_recover_init_large2
rec_recover_init_small1
rec_recover_init_small2
rec_recover_large1
rec_recover_large2
rec_recover_small1
rec_recover_small2
auto_encoder1_initial_rounds
auto_encoder2_initial_rounds
auto_encoder_init1_initial_rounds
auto_encoder_init2_initial_rounds
auto_encoder_init_large1_initial_rounds
auto_encoder_init_large2_initial_rounds
auto_encoder_init_small1_initial_rounds
auto_encoder_init_small2_initial_rounds
rec_recover1_initial_rounds
rec_recover2_initial_rounds
rec_recover_init1_initial_rounds
rec_recover_init2_initial_rounds
rec_recover_init_large1_initial_rounds
rec_recover_init_large2_initial_rounds
rec_recover_init_small1_initial_rounds
rec_recover_init_small2_initial_rounds
rec_recover_large1_initial_rounds
rec_recover_large2_initial_rounds
rec_recover_small1_initial_rounds
rec_recover_small2_initial_rounds
)

costfiles=(
output/auto_encoder1/costs.txt
output/auto_encoder2/costs.txt
output/auto_encoder_init1/costs.txt
output/auto_encoder_init2/costs.txt
output/auto_encoder_init_large1/costs.txt
output/auto_encoder_init_large2/costs.txt
output/auto_encoder_init_small1/costs.txt
output/auto_encoder_init_small2/costs.txt
output/rec_recover1/costs.txt
output/rec_recover2/costs.txt
output/rec_recover_init1/costs.txt
output/rec_recover_init2/costs.txt
output/rec_recover_init_large1/costs.txt
output/rec_recover_init_large2/costs.txt
output/rec_recover_init_small1/costs.txt
output/rec_recover_init_small2/costs.txt
output/rec_recover_large1/costs.txt
output/rec_recover_large2/costs.txt
output/rec_recover_small1/costs.txt
output/rec_recover_small2/costs.txt
output/auto_encoder1/costs.txt
output/auto_encoder2/costs.txt
output/auto_encoder_init1/costs.txt
output/auto_encoder_init2/costs.txt
output/auto_encoder_init_large1/costs.txt
output/auto_encoder_init_large2/costs.txt
output/auto_encoder_init_small1/costs.txt
output/auto_encoder_init_small2/costs.txt
output/rec_recover1/costs.txt
output/rec_recover2/costs.txt
output/rec_recover_init1/costs.txt
output/rec_recover_init2/costs.txt
output/rec_recover_init_large1/costs.txt
output/rec_recover_init_large2/costs.txt
output/rec_recover_init_small1/costs.txt
output/rec_recover_init_small2/costs.txt
output/rec_recover_large1/costs.txt
output/rec_recover_large2/costs.txt
output/rec_recover_small1/costs.txt
output/rec_recover_small2/costs.txt
)

maxranges=(
2147483647
2147483647
2147483647
2147483647
2147483647
2147483647
2147483647
2147483647
2147483647
2147483647
2147483647
2147483647
2147483647
2147483647
2147483647
2147483647
2147483647
2147483647
2147483647
2147483647
20
20
20
20
20
20
20
20
20
20
20
20
20
20
20
20
20
20
20
20
)

N=${#maxranges[@]}

echo "exp_name, minimal_x, tcost[0], vcost[0], tcost[minimal_x], vcost[minimal_x], tcost[-1], vcost[-1]" > output/minimal_xs.log
for (( i = 0; i < N; ++i )); do
	exp_name=${exp_names[i]}	
	costfile=${costfiles[i]}
	costdir=$(dirname $costfile)
	maxrange=${maxranges[i]}
	
	./plot_costs.py ${exp_name} $costfile png ${costdir}/${exp_name}.png ${maxrange} >> output/minimal_xs.log
done

