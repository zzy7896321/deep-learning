#!/usr/bin/env python

import os
import sys
from sys import exit
from utils import *
import timeit
import math

import numpy

import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams

class RecRecover(object):

    def __init__(self, 
                 input_vec,
                 target_vec,
                 n_visible,
                 n_hidden,
                 numpy_rng,
                 theano_rng = None,
                 W=None,
                 bhid=None,
                 bvis=None):

        self.n_visible = n_visible
        self.n_hidden = n_hidden

        if not theano_rng:
            theano_rng = RandomStreams(numpy_rng.randint(2 ** 30))

        
        if not W:
            init_W = numpy.asarray(
                    numpy_rng.uniform(
                        low=-4.0 * numpy.sqrt(6.0 / (n_hidden + n_visible)),
                        high=4.0 * numpy.sqrt(6.0 / (n_hidden + n_visible)),
                        size=(n_visible, n_hidden)),
                    dtype=theano.config.floatX)

            W = theano.shared(value=init_W, name="W", borrow=True)
        elif type(W) == str:
            init_W = numpy.loadtxt(W)
            W = theano.shared(value=init_W, name="W", borrow=True)

        if not bvis:
            bvis = theano.shared(
                    value=numpy.zeros(
                        n_visible,
                        dtype=theano.config.floatX), name = "b2",  borrow=True)
        elif type(bvis) == str:
            init_bvis = numpy.loadtxt(bvis)
            bvis = theano.shared(value = init_bvis, name = "b2", borrow=True)

        if not bhid:
            bhid = theano.shared(
                    value=numpy.zeros(
                        n_hidden,
                        dtype=theano.config.floatX), name = "b1", borrow=True)
        elif type(bhid) == str:
            init_bhid = numpy.loadtxt(bhid)
            bhid = theano.shared(value = init_bhid, name = "b1", borrow=True)

        
        # W1, b1 are the weights and bias from previous layer to this layer
        self.W1 = W
        self.b1 = bhid

        # W2, b2 are the weights and bias from this layer to next layer
        self.W2 = self.W1.T
        self.b2 = bvis

        self.x = input_vec
        self.y = target_vec

        self.params = [self.W1, self.b1, self.b2]

    def get_hidden_values(self, input_vec):
        return T.nnet.sigmoid(T.dot(input_vec, self.W1) + self.b1)

    def get_output(self, hidden):
        return T.nnet.sigmoid(T.dot(hidden, self.W2) + self.b2)

    def get_L2_regularization(self):
        return T.sum(self.W1 * self.W1)

    def get_L1_regularization(self):
        return T.sum(T.abs(self.W1))

    def get_mse_cost(self, x, target):
        y = self.get_output(self.get_hidden_values(x))
        
        ## MSE
        cost = T.mean(T.sum((target - y) ** 2, axis = 1))

        return cost


    def get_mse_cost_updates(self, learning_rate, reg_type, reg_rate):
        raw_cost = self.get_mse_cost(self.x, self.y) 

        if reg_type == "L1":
            cost = raw_cost + reg_rate * self.get_L1_regularization()
        elif reg_type == "L2":
            cost = raw_cost + reg_rate * self.get_L2_regularization()
        else:
            cost = raw_cost

        gradients = T.grad(cost, self.params)

        updates = [
                (param, param - learning_rate * gradients)
                for param, gradients in zip(self.params, gradients)]

        return (raw_cost, cost, updates)

    def get_cross_entropy_cost(self, x, target):
        y = self.get_output(self.get_hidden_values(x))

        ## cross entropy
        cost = T.mean(- T.sum(target * T.log(y) + (1 - target) * T.log(1 - y), axis=1)) + L2
        return cost


    def get_cross_entropy_cost_updates(self, learning_rate, reg_type, reg_rate):
        raw_cost = self.get_cross_entropy_cost(self.x, self.y)

        if reg_type == "L1":
            cost = raw_cost + reg_rate * self.get_L1_regularization()
        elif reg_type == "L2":
            cost = raw_cost + reg_rate * self.get_L2_regularization()
        else:
            cost = raw_cost

        gradients = T.grad(cost, self.params)

        updates = [
                (param, param - learning_rate * gparam)
                for param, gradients in zip(self.params, gradients)]

        return (raw_cost, cost, updates)

def parse_args(argc, argv):
    if argc < 1:
        print "usage:", argv[0], "<conffile>"
        exit(1)

    conffile = argv[1]
    fin = open(conffile, "r")
    conf = fin.read()
    fin.close()
    exec(conf)

    return (pixels, 
            trainfile, 
            trainlabelfile, 
            costfunc, 
            batch_size, 
            learning_rate, 
            training_epochs, 
            output_dir, 
            output_round, 
            validfile, 
            validlabelfile,
            reg_type, 
            reg_rate,
            Wfile,
            b1file,
            b2file)
    

def read_rectangles(pixels, inputfile):
    fin = open(inputfile, "r")
    data = fin.readlines() ## x1 x2 y1 y2 on each line
    fin.close()
    
    data = map(lambda line: map(lambda s: eval(s), line.split(' ')), data)
    N = len(data)
    X = numpy.zeros((N, pixels, pixels), dtype = theano.config.floatX)
    
    for i in xrange(N):
        for x in xrange(data[i][0], data[i][1] + 1):
            for y in xrange(data[i][2], data[i][2] + 1):
                X[i][x][y] = 1.0

    X.shape = (N, -1)
    shared_x = theano.shared(X, borrow = True)

    return shared_x

def dump_weights(auto_encoder, epoch):
    W = auto_encoder.W1.get_value(borrow=True)
    b1 = auto_encoder.b1.get_value(borrow=True)
    b2 = auto_encoder.b2.get_value(borrow=True)

    numpy.savetxt("epoch_%d_W.txt" % epoch, W, header="epoch %d weights" % epoch)    
    numpy.savetxt("epoch_%d_b1.txt" % epoch, b1, header="epoch %d b1" % epoch)
    numpy.savetxt("epoch_%d_b2.txt" % epoch, b2, header="epoch %d b2" % epoch)

def main(argc, argv):
    (pixels, 
     inputfile, 
     inputlabelfile,
     costfunc, 
     batch_size, 
     learning_rate, 
     training_epochs,
     output_dir,
     output_round,
     validfile,
     validlabelfile,
     reg_type,
     reg_rate,
     Wfile,
     b1file,
     b2file) = parse_args(argc, argv) 
    
    ## load data and parameters 
    training_set_x = read_rectangles(pixels, inputfile)
    valid_set_x = read_rectangles(pixels, validfile)
    N = training_set_x.get_value(borrow=True).shape[0]
    Nvalid = valid_set_x.get_value(borrow=True).shape[0]

    training_set_y = read_rectangles(pixels, inputlabelfile)
    valid_set_y = read_rectangles(pixels, validlabelfile)
    
    if training_set_y.get_value(borrow=True).shape[0] != N:
        print "[error] the number of training data and labels mismatch"
        exit(1)

    if valid_set_y.get_value(borrow=True).shape[0] != Nvalid:
        print "[error] the number of validation data and labels mismatch"
        exit(1)

    if batch_size <= 0 or batch_size > N:
        batch_size = N

    n_batches = (N + batch_size - 1) / batch_size

    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    back_dir = os.getcwd()

    ## set up the model
    rng = numpy.random.RandomState()
    theano_rng = RandomStreams(rng.randint(2 ** 30))

    index = T.lscalar()
    X = T.matrix('X')
    Y = T.matrix('Y')

    auto_encoder = RecRecover(input_vec = X,
                              target_vec = Y,
                               n_visible = pixels * pixels,
                               n_hidden = int(math.sqrt(pixels * pixels)),
                               numpy_rng = rng,
                               theano_rng = theano_rng,
                               W = Wfile,
                               bhid = b1file,
                               bvis = b2file)

    if costfunc == "MSE":
        raw_cost, cost, updates = auto_encoder.get_mse_cost_updates(learning_rate, reg_type, reg_rate)
        valid_cost = auto_encoder.get_mse_cost(valid_set_x, valid_set_y)
        init_cost = auto_encoder.get_mse_cost(training_set_x, training_set_y)
    else:
        raw_cost, cost, updates = auto_encoder.get_cross_entropy_cost_updates(learning_rate, reg_type, reg_rate)
        valid_cost = auto_encoder.get_cross_entropy_cost(valid_set_x, valid_set_y)
        init_cost = auto_encoder.get_cross_entropy_cost(traininig_set_x, training_set_y) 

    train_auto_encoder = theano.function(
            [index],
            [raw_cost, cost],
            updates=updates,
            givens = {
                X: training_set_x[index * batch_size : 
                    ((index + 1) * batch_size <= N) * ((index + 1) * batch_size)  + 
                    ((index + 1) * batch_size > N) * N],
                Y: training_set_y[index * batch_size : 
                    ((index + 1) * batch_size <= N) * ((index + 1) * batch_size) +
                    ((index + 1) * batch_size > N) * N]
            })
    
    
    calc_init_cost = theano.function([], init_cost)
    calc_valid_cost = theano.function([], valid_cost)

    ## debug output
    print "Rectangle Recover"
    print "N =", N
    print "Nvalid =", Nvalid
    print "n_visible =", auto_encoder.n_visible
    print "n_hidden =", auto_encoder.n_hidden
    print "batch size =", batch_size
    print "learning rate =", learning_rate
    print "training epochs =", training_epochs
    print "cost func =", costfunc
    print "output dir =", output_dir
    print "output round =", output_round
    print "reg_type =", reg_type
    print "reg_rate =", reg_rate
    
    ## training
    
    os.chdir(output_dir)

    init_cost = calc_init_cost()
    valid_cost = calc_valid_cost()
    print "init training cost = %f, valid cost %f" % (init_cost, valid_cost)

    total_time = 0
    start_time = timeit.default_timer()

    for epoch in xrange(training_epochs):
        c = []
        rc = []
        for batch_index in xrange(n_batches):
            raw_cost, cost = train_auto_encoder(batch_index)
            c.append(cost)
            rc.append(raw_cost)

        valid_cost = calc_valid_cost()

        end_time = timeit.default_timer()
        total_time = end_time - start_time

        print "Training epoch %d, raw_cost %f, training cost %f, time %s, valid cost %f" % ( 
            epoch, 
            numpy.mean(rc),
            numpy.mean(c), 
            format_time_str(total_time),
            valid_cost
            )
        if epoch % output_round == output_round - 1:
            dump_weights(auto_encoder, epoch)

    print "Total time: %s" % format_time_str(total_time)
    os.chdir(back_dir)

if __name__ == "__main__":
    main(len(sys.argv) - 1, sys.argv)

