#!/usr/bin/env python
    
def read_line(fname):
    fin = open(fname, "r")
    l = fin.readline()
    fin.close()

    return l.rstrip('\n\r').split(' ')[-1]


fout = open("output/extra/extra.log", "w")
fout.write(", p = 0.01, p = 0.1, p = 0.2\n")
for i in xrange(499, 5000, 500):
    line = str(i)
    line += ", " + read_line("output/extra/auto_encoder_init_small1/%d/costs.txt" % i)
    line += ", " + read_line("output/extra/auto_encoder_init1/%d/costs.txt" % i)
    line += ", " + read_line("output/extra/auto_encoder_init_large1/%d/costs.txt" % i)

    fout.write(line + "\n")
fout.close()
