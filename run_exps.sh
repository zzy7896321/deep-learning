#!/bin/bash

CONFS="
	conf/auto_encoder/auto_encoder1.conf
	conf/auto_encoder/auto_encoder2.conf	
	conf/rec_recover/rec_recover1.conf	
	conf/rec_recover/rec_recover2.conf	
	conf/auto_encoder/auto_encoder_init1.conf	
	conf/auto_encoder/auto_encoder_init2.conf	
	conf/rec_recover/rec_recover_init1.conf 
	conf/rec_recover/rec_recover_init2.conf
	conf/rec_recover_small/rec_recover1.conf	
	conf/rec_recover_small/rec_recover2.conf
	conf/rec_recover_small/rec_recover_init1.conf
	conf/rec_recover_small/rec_recover_init2.conf
	conf/auto_encoder/auto_encoder_init_small1.conf
	conf/auto_encoder/auto_encoder_init_small2.conf
	conf/rec_recover_large/rec_recover1.conf
	conf/rec_recover_large/rec_recover2.conf
	conf/rec_recover_large/rec_recover_init1.conf
	conf/rec_recover_large/rec_recover_init2.conf
	conf/auto_encoder/auto_encoder_init_large1.conf
	conf/auto_encoder/auto_encoder_init_large2.conf
	conf/extra/*/*.conf"


BASEDIR="$(cd `dirname $0`; pwd)"

for conf in $CONFS; do
	./run_exp.sh $conf
done

