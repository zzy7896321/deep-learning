#!/bin/bash

if [[ $# < 1 ]]; then
	echo "usage: $0 <conf>"
	exit 1
fi

BASEDIR="$(cd `dirname $0`; pwd)"
conf=$1

PROG="$(sed -n 's/^#[ \t]*\([a-z_A-Z][a-z_A-Z]*\)[ \t]*/\1/p' $conf)"
CMD="${BASEDIR}/${PROG}"	
OUTPUTDIR="$(sed -n 's/^[ \t]*output_dir[ \t]*=[ \t]*"\(.*\)"[ \t]*$/\1/p' $conf)"

echo "$PROG $conf"
echo $OUTPUTDIR
mkdir -p $OUTPUTDIR
	
unbuffer "$CMD" $conf | tee ${OUTPUTDIR}/LOGS
./grep_costs.sh ${OUTPUTDIR}/LOGS > ${OUTPUTDIR}/costs.txt
