#!/usr/bin/env python

import sys
from sys import exit
import random
import os
import os.path
from utils import *

def main(argc, argv):
    if argc < 5:
        print "usage:", argv[0], "<pixels> <prob. masked> <inputfile> <outputfile> <labelfile>"
        exit(1)
    
    pixels = parse_positive_int_arg("pixels", argv[1])
    prob_masked = parse_float_arg("prob. masked", argv[2], lambda x : x >= 0 and x <= 1)
    inputfile = argv[3]
    outputfile = argv[4]
    labelfile = argv[5]

    random.seed()
    
    fin = open(inputfile, "r")
    fout = open(outputfile, "w")
    fout2 = open(labelfile, "w")
    
    for line in fin:
        x1, x2, y1, y2 = map(lambda s: eval(s), line.split(' '))
             
        for x in xrange(pixels):
            for y in xrange(pixels):
                if x < x1 or x > x2 or y < y1 or y > y2:
                    value = 0
                    orig = 0
                else:
                    orig = 1
                    if random.random() < prob_masked:
                        value = 0
                    else:
                        value = 1
                if y == pixels - 1:
                    fout.write("%d\n" % (value))
                    fout2.write("%d\n" % (orig))
                else:
                    fout.write("%d " % (value))
                    fout2.write("%d " % (orig))
    
    fout2.close()
    fout.close()
    fin.close()

if __name__== "__main__":
    main(len(sys.argv) - 1, sys.argv)
