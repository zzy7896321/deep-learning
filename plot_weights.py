#!/usr/bin/env python

import sys
from sys import exit
from utils import *

import numpy

import matplotlib.pyplot as plt

def main(argc, argv):
    
    if argc < 3:
        print "usage:", argv[0], "<weight file> <outputfmt> <outputfile>"
        exit(1)

    weight_file = argv[1]
    outputfmt = argv[2]
    outputfile = argv[3]


    mat = numpy.loadtxt(weight_file)
    mat.shape = (-1)
    mat = numpy.sort(abs(mat))

    N = mat.shape[0]

    plt.scatter(range(N), mat)
    plt.savefig(outputfile, dpi=480, format=outputfmt)

if __name__ == "__main__":
    main(len(sys.argv) - 1, sys.argv)
